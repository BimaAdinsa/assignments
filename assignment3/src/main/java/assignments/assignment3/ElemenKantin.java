package assignments.assignment3;

class ElemenKantin extends ElemenFasilkom {

    private Makanan[] daftarMakanan = new Makanan[10]; 

    public ElemenKantin(String nama) {
        super("Kantin", nama, 0);
    }

    public Makanan[] getDaftarMakanan(){
        return this.daftarMakanan;
    }

    public void setMakanan(String nama, long harga) {
        boolean ada = false;
        for (int i = 0; i<daftarMakanan.length; i++){
            if (daftarMakanan[i] != null && daftarMakanan[i].toString().equals(nama)){
                ada = true;
            }
        }
        if (ada){
            System.out.println("[DITOLAK] " + nama + " sudah pernah terdaftar");
        }
        else{
            System.out.println(this.toString() + " telah mendaftarkan makanan " + nama +
            " dengan harga " + harga);
            for (int i=0; i<daftarMakanan.length;i++){
                if (daftarMakanan[i] == null){
                    Makanan makanan = new Makanan(nama, harga); 
                    daftarMakanan[i] = makanan; 
                    break;
                }
            }
        }
    }
}