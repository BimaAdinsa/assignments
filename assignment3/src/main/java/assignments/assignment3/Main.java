package assignments.assignment3;

import java.util.Scanner;

public class Main {
    private static ElemenFasilkom[] daftarElemenFasilkom = new ElemenFasilkom[100]; 
    private static MataKuliah[] daftarMataKuliah = new MataKuliah[100];
    private static int totalMataKuliah = 0;
    private static int totalElemenFasilkom = 0; 

    public static void addMahasiswa(String nama, long npm) {
        Mahasiswa mhs = new Mahasiswa(nama, npm); 
        daftarElemenFasilkom[totalElemenFasilkom] = mhs; 
        totalElemenFasilkom++;
        System.out.println(nama + " berhasil ditambahkan");
    }

    public static void addDosen(String nama) {
        Dosen dosen = new Dosen(nama);
        daftarElemenFasilkom[totalElemenFasilkom] = dosen;
        totalElemenFasilkom++;
        System.out.println(nama + " berhasil ditambahkan");
    }

    public static void addElemenKantin(String nama) {
        ElemenKantin elemenKantin = new ElemenKantin(nama); 
        daftarElemenFasilkom[totalElemenFasilkom] = elemenKantin;
        totalElemenFasilkom++;
        System.out.println(nama + " berhasil ditambahkan");
    }

    public static void menyapa(String objek1, String objek2) {
       if (objek1.equals(objek2)){
           System.out.println("[DITOLAK] Objek yang sama tidak bisa saling menyapa");
       }
       
       else{
           for (int i=0;i<daftarElemenFasilkom.length;i++){
               if (daftarElemenFasilkom[i] != null && daftarElemenFasilkom[i].toString().equals(objek1)){
                   for (int j = 0; j<daftarElemenFasilkom.length; j++){
                       if (daftarElemenFasilkom[j] != null && daftarElemenFasilkom[j].toString().equals(objek2)){
                           daftarElemenFasilkom[i].menyapa(daftarElemenFasilkom[j]); 
                           break;
                       }
                   }
                   break;
               }
           }
       }
    }

    public static void addMakanan(String objek, String namaMakanan, long harga) {
        for (int i=0;i<daftarElemenFasilkom.length;i++){
            if (daftarElemenFasilkom[i] != null && daftarElemenFasilkom[i].toString().equals(objek)){
                if (daftarElemenFasilkom[i].getTipe() != "Kantin"){
                    System.out.println("[DITOLAK] " + objek + " bukan merupakan elemen kantin");
                }
                else{
                    ElemenKantin kantin = (ElemenKantin) daftarElemenFasilkom[i]; 
                    kantin.setMakanan(namaMakanan, harga);
                }
            }
        }
    }

    public static void membeliMakanan(String objek1, String objek2, String namaMakanan) {
        for (int i=0;i<daftarElemenFasilkom.length;i++){
            if (daftarElemenFasilkom[i] != null && daftarElemenFasilkom[i].toString().equals(objek2)){
                if (daftarElemenFasilkom[i].getTipe() != "Kantin"){
                    System.out.println("[DITOLAK] Hanya elemen kantin yang dapat menjual makanan");
                }
                else{
                    if (daftarElemenFasilkom[i].toString().equals(objek1)){
                        System.out.println("[DITOLAK] Elemen kantin tidak bisa membeli makanan sendiri");
                    }
                    else{
                        for (int j=0;j<daftarElemenFasilkom.length;j++){
                            if (daftarElemenFasilkom[j] != null && daftarElemenFasilkom[j].toString().equals(objek1)){
                                daftarElemenFasilkom[j].membeliMakanan(daftarElemenFasilkom[j], daftarElemenFasilkom[i], namaMakanan);
                                break;
                            }
                        }
                    }
                }
            }
        }
    }

    public static void createMatkul(String nama, int kapasitas) {
        MataKuliah matkul = new MataKuliah(nama, kapasitas);
        daftarMataKuliah[totalMataKuliah] = matkul;
        totalMataKuliah++;
        System.out.println(nama + " berhasil ditambahkan dengan kapasitas " + kapasitas);
    }

    public static void addMatkul(String objek, String namaMataKuliah) {
        for (int i=0; i<daftarElemenFasilkom.length;i++){
            if (daftarElemenFasilkom[i] != null && daftarElemenFasilkom[i].toString().equals(objek)){
                if (daftarElemenFasilkom[i].getTipe() != "Mahasiswa"){
                    System.out.println("[DITOLAK] Hanya mahasiswa yang dapat menambahkan matkul");
                }
                else{
                    Mahasiswa mahasiswa = (Mahasiswa) daftarElemenFasilkom[i]; 
                    for (int j=0; j<daftarMataKuliah.length; j++){
                        if (daftarMataKuliah[j] != null && daftarMataKuliah[j].toString().equals(namaMataKuliah)){
                            mahasiswa.addMatkul(daftarMataKuliah[j]); 
                        }
                    }
                }
            }
        }
    }

    public static void dropMatkul(String objek, String namaMataKuliah) {
        for (int i=0; i<daftarElemenFasilkom.length;i++){
            if (daftarElemenFasilkom[i] != null && daftarElemenFasilkom[i].toString().equals(objek)){
                if (daftarElemenFasilkom[i].getTipe() != "Mahasiswa"){
                    System.out.println("[DITOLAK] Hanya mahasiswa yang dapat drop matkul");
                }
                else{
                    Mahasiswa mahasiswa = (Mahasiswa) daftarElemenFasilkom[i];  
                    for (int j=0; j<daftarMataKuliah.length; j++){
                        if (daftarMataKuliah[j] != null && daftarMataKuliah[j].toString().equals(namaMataKuliah)){
                            mahasiswa.dropMatkul(daftarMataKuliah[j]); 
                        }
                    }
                }
            }
        }

    }

    public static void mengajarMatkul(String objek, String namaMataKuliah) {
        for (int i = 0; i<daftarElemenFasilkom.length;i++){
            if (daftarElemenFasilkom[i] != null && daftarElemenFasilkom[i].toString().equals(objek)){
                if (daftarElemenFasilkom[i].getTipe() != "Dosen"){
                    System.out.println("[DITOLAK] Hanya dosen yang dapat mengajar matkul");
                }
                else{
                    Dosen dosen = (Dosen) daftarElemenFasilkom[i];  
                    for (int j=0; j<daftarMataKuliah.length; j++){
                        if (daftarMataKuliah[j] != null && daftarMataKuliah[j].toString().equals(namaMataKuliah)){
                            dosen.mengajarMataKuliah(daftarMataKuliah[j]); 
                        }
                    } 
                }
            }
        }
        
    }

    public static void berhentiMengajar(String objek) {
        for (int i = 0; i<daftarElemenFasilkom.length;i++){
            if (daftarElemenFasilkom[i] != null && daftarElemenFasilkom[i].toString().equals(objek)){
                if (daftarElemenFasilkom[i].getTipe() != "Dosen"){
                    System.out.println("[DITOLAK] Hanya dosen yang dapat berhenti mengajar");
                }
                else{
                    Dosen dosen = (Dosen) daftarElemenFasilkom[i];
                    dosen.dropMataKuliah();
                }
            }
        } 
    }

    public static void ringkasanMahasiswa(String objek) {
        for (int i=0; i<daftarElemenFasilkom.length;i++){
            if (daftarElemenFasilkom[i] != null && daftarElemenFasilkom[i].toString().equals(objek)){
                if (daftarElemenFasilkom[i].getTipe() != "Mahasiswa"){
                    System.out.println("[DITOLAK] " + objek + " bukan merupakan seorang mahasiswa");
                    break;
                }
                else{
                    Mahasiswa mahasiswa = (Mahasiswa) daftarElemenFasilkom[i];
                    System.out.println("Nama: " + objek);
                    System.out.println("Tanggal lahir: " + mahasiswa.extractTanggalLahir(mahasiswa.getNpm()));
                    System.out.println("Jurusan: " + mahasiswa.extractJurusan(mahasiswa.getNpm()));
                    System.out.println("Daftar Mata Kuliah: ");

                    if (mahasiswa.getJumlahMataKuliah() == 0){
                        System.out.println("Belum ada mata kuliah yang diambil");
                    }
                    else{
                        for(int j=0, k=0; j<mahasiswa.getDaftarMataKuliah().length; j++){
                            if (mahasiswa.getDaftarMataKuliah()[j] != null){
                                System.out.println ((k+1) + ". " + mahasiswa.getDaftarMataKuliah()[j]);
                                k++;
                            }
                        }
                    }
                }
            }
        }
    }

    public static void ringkasanMataKuliah(String namaMataKuliah) {
        for (int i=0; i<daftarMataKuliah.length; i++){
            if (daftarMataKuliah[i] != null && daftarMataKuliah[i].toString().equals(namaMataKuliah)){
                System.out.println("Nama mata kuliah: " + namaMataKuliah);
                System.out.println("Jumlah mahasiswa: " + daftarMataKuliah[i].getJumlahMahasiswa());
                System.out.println("Kapasitas: " + daftarMataKuliah[i].getKapasitas());
                if (daftarMataKuliah[i].getDosen() != null){
                    System.out.println("Dosen pengajar: " + daftarMataKuliah[i].getDosen());
                }
                else{
                    System.out.println("Dosen pengajar: Belum ada");
                }
                System.out.println("Daftar mahasiswa yang mengambil mata kuliah ini: ");

                if (daftarMataKuliah[i].getJumlahMahasiswa() == 0){
                    System.out.println("Belum ada mahasiswa yang mengambil mata kuliah ini");
                }
                else{
                    
                    for (int j = 0, k=0; j < daftarMataKuliah[i].getDaftarMahasiswa().length; j++){
                        if (daftarMataKuliah[i].getDaftarMahasiswa()[j] != null){
                            System.out.println((k+1) + ". " + daftarMataKuliah[i].getDaftarMahasiswa()[j]);
                            k++;
                        }
                    }
                }
            }
        }
    }

    public static void nextDay() {
        for (int i=0;i<daftarElemenFasilkom.length;i++){
            if(daftarElemenFasilkom[i] != null && (daftarElemenFasilkom[i].getJumlahDisapa() >= ((totalElemenFasilkom-1)/2))){
                daftarElemenFasilkom[i].setFriendship(daftarElemenFasilkom[i].getFriendship()+10);
            }
            else if (daftarElemenFasilkom[i] != null && (daftarElemenFasilkom[i].getJumlahDisapa() <((totalElemenFasilkom-1)/2))){
                daftarElemenFasilkom[i].setFriendship(daftarElemenFasilkom[i].getFriendship()-5);
            }
        }
        System.out.println("Hari telah berakhir dan nilai friendship telah diupdate");
        friendshipRanking();
        for (int i=0; i<daftarElemenFasilkom.length;i++){
            if (daftarElemenFasilkom[i] != null){
                daftarElemenFasilkom[i].resetMenyapa(); 
            }
        }
        
    }

    public static void friendshipRanking() {
        ElemenFasilkom sementara = null;
        int index;
        for (int i=0; i<daftarElemenFasilkom.length;i++){
            index = i;
            for (int j=i+1; j<daftarElemenFasilkom.length;j++){
                if (daftarElemenFasilkom[j] != null && (daftarElemenFasilkom[j].getFriendship() > daftarElemenFasilkom[index].getFriendship())){
                    index = j;  
                }
                else if (daftarElemenFasilkom[j] != null && (daftarElemenFasilkom[j].getFriendship() == daftarElemenFasilkom[index].getFriendship())){
                    if ((int) daftarElemenFasilkom[j].toString().charAt(0) < (int) daftarElemenFasilkom[index].toString().charAt(0)){
                        index = j;
                    }
                }
            }
            sementara = daftarElemenFasilkom[index];
            daftarElemenFasilkom[index] = daftarElemenFasilkom[i];
            daftarElemenFasilkom[i] = sementara;
            }
    
        for (int i=0 , j=0; i<daftarElemenFasilkom.length;i++){
            if (daftarElemenFasilkom[i] != null){
                System.out.println((j+1) + ". " + daftarElemenFasilkom[i].toString() +"(" + daftarElemenFasilkom[i].getFriendship() + ")");
                j++;
            }
        }

    }

    public static void programEnd() {
        System.out.println("Program telah berakhir. Berikut nilai terakhir dari friendship pada Fasilkom :");
        friendshipRanking();
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        while (true) {
            String in = input.nextLine();
            if (in.split(" ")[0].equals("ADD_MAHASISWA")) {
                addMahasiswa(in.split(" ")[1], Long.parseLong(in.split(" ")[2]));
            } else if (in.split(" ")[0].equals("ADD_DOSEN")) {
                addDosen(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("ADD_ELEMEN_KANTIN")) {
                addElemenKantin(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("MENYAPA")) {
                menyapa(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("ADD_MAKANAN")) {
                addMakanan(in.split(" ")[1], in.split(" ")[2], Long.parseLong(in.split(" ")[3]));
            } else if (in.split(" ")[0].equals("MEMBELI_MAKANAN")) {
                membeliMakanan(in.split(" ")[1], in.split(" ")[2], in.split(" ")[3]);
            } else if (in.split(" ")[0].equals("CREATE_MATKUL")) {
                createMatkul(in.split(" ")[1], Integer.parseInt(in.split(" ")[2]));
            } else if (in.split(" ")[0].equals("ADD_MATKUL")) {
                addMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("DROP_MATKUL")) {
                dropMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("MENGAJAR_MATKUL")) {
                mengajarMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("BERHENTI_MENGAJAR")) {
                berhentiMengajar(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("RINGKASAN_MAHASISWA")) {
                ringkasanMahasiswa(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("RINGKASAN_MATKUL")) {
                ringkasanMataKuliah(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("NEXT_DAY")) {
                nextDay();
            } else if (in.split(" ")[0].equals("PROGRAM_END")) {
                programEnd();
                break;
            }
        }
        input.close();
    }
}