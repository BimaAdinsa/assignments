package assignments.assignment3;

abstract class ElemenFasilkom {
    private String tipe;
    private String nama;  
    private int friendship; 
    private  ElemenFasilkom[] telahMenyapa; 
    private  int jumlahDisapa;  

    public ElemenFasilkom(String tipe, String nama, int friendship){
        this.tipe = tipe;
        this.nama = nama;
        this.friendship = friendship;
        this.telahMenyapa = new ElemenFasilkom[100];
        jumlahDisapa = 0;
    }

    public String getTipe(){
        return this.tipe;
    }
    
    public ElemenFasilkom[] getTelahMenyapa(){
        return this.telahMenyapa;
    }

    public int getJumlahDisapa(){
        return this.jumlahDisapa;
    }

    public int getFriendship(){
        return this.friendship;
    }

    public void setFriendship(int friendship){
        this.friendship = friendship;
        if (this.friendship <0){
            this.friendship = 0; 
        }
        else if (this.friendship > 100){
            this.friendship = 100; 
        }
    }


    public void menyapa(ElemenFasilkom elemenFasilkom) {
        boolean ada = false;
        for (int i = 0; i<telahMenyapa.length; i++){
            if (this.telahMenyapa[i] != null && this.telahMenyapa[i].toString().equals(elemenFasilkom.toString())){
                ada = true;
            }
            else if (this.telahMenyapa[i] != null && elemenFasilkom.toString().equals(this.telahMenyapa[i].toString())){
                ada = true;
            }
        }
        if (ada){
            System.out.println("[DITOLAK] " + this.nama + " telah menyapa " + 
            elemenFasilkom.toString() + " hari ini");
        }
        else{
            this.telahMenyapa[jumlahDisapa] = elemenFasilkom;  
            elemenFasilkom.telahMenyapa[elemenFasilkom.jumlahDisapa] = this;   
            this.jumlahDisapa++;  
            elemenFasilkom.jumlahDisapa++;  
            System.out.println(this.nama + " menyapa dengan " + elemenFasilkom.toString());

            
            if (this.getTipe() == "Mahasiswa" && elemenFasilkom.getTipe() == "Dosen"){
                Mahasiswa mhs = (Mahasiswa) this; 
                Dosen dosen = (Dosen) elemenFasilkom; 
                for (int i=0;i<mhs.getDaftarMataKuliah().length;i++){
                    if (mhs.getDaftarMataKuliah()[i] != null && dosen.getMataKuliah() != null && (mhs.getDaftarMataKuliah()[i].toString().equals(dosen.getMataKuliah().toString()))){
                        mhs.setFriendship(mhs.getFriendship()+2);
                        dosen.setFriendship(dosen.getFriendship()+2);
                    }
                }
            }
            
            else if (this.getTipe() == "Dosen" && elemenFasilkom.getTipe() == "Mahasiswa"){
                Mahasiswa mhs = (Mahasiswa) elemenFasilkom;
                Dosen dosen = (Dosen) this; 
                for (int i=0;i<mhs.getDaftarMataKuliah().length;i++){
                    if (mhs.getDaftarMataKuliah()[i] != null && dosen.getMataKuliah() != null && (mhs.getDaftarMataKuliah()[i].toString() == dosen.getMataKuliah().toString())){
                        mhs.setFriendship(mhs.getFriendship()+2);
                        dosen.setFriendship(dosen.getFriendship()+2);
                    }
                }
            }
        }
    }

    public void resetMenyapa() {
        for (int i=0; i<telahMenyapa.length;i++){
            telahMenyapa[i] = null;
        }
        jumlahDisapa = 0;
    }
    
    public void membeliMakanan(ElemenFasilkom pembeli, ElemenFasilkom penjual, String namaMakanan) {
        ElemenKantin kantin = (ElemenKantin) penjual;   
        
        boolean jual = false;
        Makanan makanan = null;
        for (int i=0; i<kantin.getDaftarMakanan().length; i++){
            if (kantin.getDaftarMakanan()[i] != null && (kantin.getDaftarMakanan()[i]).toString().equals(namaMakanan)){
                jual = true;
                makanan = kantin.getDaftarMakanan()[i];
            }
        }      
        if (jual){
            System.out.println(pembeli.toString() + " berhasil membeli " + namaMakanan + " seharga " + makanan.getHarga());
            penjual.setFriendship(penjual.getFriendship()+1);
            pembeli.setFriendship(pembeli.getFriendship()+1);
            
        }
        else{
            System.out.println("[DITOLAK] "+ penjual.toString() + " tidak menjual " + namaMakanan);
        }
    }

    public String toString() {
        return this.nama;
    }
}