package assignments.assignment3;

class MataKuliah {
    private String nama;    
    private int kapasitas; 
    private Dosen dosen;    
    private Mahasiswa[] daftarMahasiswa;   
    private int jumlahMahasiswa;   

    public MataKuliah(String nama, int kapasitas) {
        this.nama = nama;
        this.kapasitas = kapasitas;
        this.daftarMahasiswa = new Mahasiswa[kapasitas];
        this.jumlahMahasiswa = 0;
    }

    public int getKapasitas(){
        return this.kapasitas;
    }
    public Dosen getDosen(){
        return this.dosen;
    }
    public Mahasiswa[] getDaftarMahasiswa(){
        return this.daftarMahasiswa;
    }
    public int getJumlahMahasiswa(){
        return this.jumlahMahasiswa;
    }

    public void addMahasiswa(Mahasiswa mahasiswa) {
        daftarMahasiswa[jumlahMahasiswa] = mahasiswa;
        jumlahMahasiswa++;
    }

    public void dropMahasiswa(Mahasiswa mahasiswa) {
        Mahasiswa[] mahasiswaFix = new Mahasiswa [kapasitas];

        for(int i=0; i < daftarMahasiswa.length; i++){
            if(daftarMahasiswa[i] != null && daftarMahasiswa[i] != mahasiswa){
                mahasiswaFix[i] = daftarMahasiswa[i];
            }
        }
        this.daftarMahasiswa = mahasiswaFix; 
        jumlahMahasiswa --;
    }

    public void addDosen(Dosen dosen) {
        this.dosen = dosen;
    }

    public void dropDosen() {
        this.dosen = null;
    }

    public String toString() {
        return this.nama;
    }
}