package assignments.assignment3;

class Mahasiswa extends ElemenFasilkom {
    private MataKuliah[] daftarMataKuliah = new MataKuliah[10]; 
    private long npm;   
    private String tanggalLahir;  
    private String jurusan; 
    private int jumlahMataKuliah;   

    public Mahasiswa(String nama, long npm) {
        super("Mahasiswa", nama, 0);
        this.npm = npm;
        jumlahMataKuliah = 0;
    }

    public long getNpm(){
        return this.npm;
    }

    public MataKuliah[] getDaftarMataKuliah(){
        return this.daftarMataKuliah;
    }

    public int getJumlahMataKuliah(){
        return this.jumlahMataKuliah;
    }

    public void addMatkul(MataKuliah mataKuliah) {
        boolean matkulSudahDiambil = false;
        for (int i=0; i < daftarMataKuliah.length; i++){
            if (daftarMataKuliah[i] != null && daftarMataKuliah[i] == mataKuliah){
                System.out.println("[DITOLAK] " + mataKuliah.toString() + " telah diambil sebelumnya");
                matkulSudahDiambil = true;
                break;
            }
        }
        if (matkulSudahDiambil == false){
            if (mataKuliah.getJumlahMahasiswa() == mataKuliah.getKapasitas()){
                System.out.println ("[DITOLAK] " + mataKuliah.toString() + " telah penuh kapasitasnya");
            }
            else{
                System.out.println(this.toString() + " berhasil menambahkan mata kuliah " + mataKuliah.toString());
                daftarMataKuliah[jumlahMataKuliah] = mataKuliah;
                jumlahMataKuliah++;
                mataKuliah.addMahasiswa(this);  
            }
        }

    }

    public void dropMatkul(MataKuliah mataKuliah) {
        MataKuliah[] matkulFix = new MataKuliah [10];
        for (int i=0; i < daftarMataKuliah.length; i++) {
            if (daftarMataKuliah[i] != null && daftarMataKuliah[i] != mataKuliah) {
                matkulFix[i] = daftarMataKuliah[i];
            }
        }

        boolean udahPernah = false; 
        for (int i=0; i<daftarMataKuliah.length;i++){
            if (daftarMataKuliah[i] == mataKuliah) {
                udahPernah = true; 
            }
        }
        boolean bisaDrop = true;
        if (udahPernah == false){
            System.out.println("[DITOLAK] " + mataKuliah.toString() + " belum pernah diambil");
            bisaDrop = false; 
        }

        if (bisaDrop == true){
            mataKuliah.dropMahasiswa(this); 
            jumlahMataKuliah--;
            System.out.println(this.toString() + " berhasil drop mata kuliah " + mataKuliah.toString());
        }

        this.daftarMataKuliah = matkulFix;  
    }

    public String extractTanggalLahir(long npm) {
        this.tanggalLahir = (Integer.parseInt(Long.toString(npm).substring(4,6)) + "-" + 
        Integer.parseInt(Long.toString(npm).substring(6,8)) + "-" + 
        Integer.parseInt(Long.toString(npm).substring(8,12)));
        return this.tanggalLahir;
    }

    public String extractJurusan(long npm) {
        String kodeJurusan = Long.toString(npm).substring(2,4);
        if (kodeJurusan.equals("01")){
             this.jurusan = "Ilmu Komputer";
         }
        else{
             this.jurusan = "Sistem Informasi";
         }
        return this.jurusan;
    }
}