package assignments.assignment3;

class Dosen extends ElemenFasilkom {
    private MataKuliah mataKuliah; 

    public Dosen (String nama) {
        super("Dosen", nama, 0);
    }

    public MataKuliah getMataKuliah(){
        return this.mataKuliah;
    }

    public void mengajarMataKuliah(MataKuliah mataKuliah) {
        if (this.mataKuliah != null){
            System.out.println("[DITOLAK] " + this.toString() + " sudah mengajar mata kuliah " +
            this.mataKuliah);
        }
        else{
            if (mataKuliah.getDosen() != null){
                System.out.println("[DITOLAK] " + mataKuliah.toString() + " sudah memiliki dosen pengajar");
            }
            else{
                System.out.println(this.toString() + " mengajar mata kuliah " + mataKuliah.toString());
                mataKuliah.addDosen(this); 
                this.mataKuliah = mataKuliah; 
            }
        }    
    }

    public void dropMataKuliah() {
        if (this.mataKuliah != null){
            System.out.println(this.toString() + " berhenti mengajar " + mataKuliah.toString());
            mataKuliah.dropDosen(); 
            this.mataKuliah = null; 
        }
        else{
            System.out.println("[DITOLAK] " + this.toString() + " sedang tidak mengajar mata kuliah apapun");
        }
    }
}