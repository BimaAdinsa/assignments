package assignments.assignment2;

public class MataKuliah {
    private String kode;
    private String nama;
    private int sks;
    private int kapasitas;
    private Mahasiswa[] daftarMahasiswa;
    private int jumlahMahasiswa;

    public MataKuliah(String kode, String nama, int sks, int kapasitas){
        this.kode = kode;
        this.nama = nama;
        this.sks = sks;
        this.kapasitas = kapasitas;
        jumlahMahasiswa = 0;  
        daftarMahasiswa = new Mahasiswa [kapasitas];
    }

    public String getKode(){
        return this.kode;
    }

    public String getNama(){
        return this.nama;
    }

    public int getSks(){
        return this.sks;
    }

    public int getKapasitas(){
        return this.kapasitas;
    }

    public Mahasiswa[] getDaftarMahasiswa(){
        return this.daftarMahasiswa;
    }

    public int getJumlahMahasiswa(){
        return this.jumlahMahasiswa;
    }

    public void addMahasiswa(Mahasiswa mahasiswa) {
        daftarMahasiswa[jumlahMahasiswa] = mahasiswa; 
        jumlahMahasiswa++; 
    }

    public void dropMahasiswa(Mahasiswa mahasiswa) {
        Mahasiswa[] mahasiswaNew = new Mahasiswa [kapasitas];

        for(int i=0; i < daftarMahasiswa.length; i++){
            if(daftarMahasiswa[i] != null && daftarMahasiswa[i] != mahasiswa){
                mahasiswaNew[i] = daftarMahasiswa[i];
            }
        }
        this.daftarMahasiswa = mahasiswaNew; 
        jumlahMahasiswa --;
    }

    public String toString() {
        return this.nama;
    }
}
