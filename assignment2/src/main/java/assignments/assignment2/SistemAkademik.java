package assignments.assignment2;

import java.util.Scanner;

public class SistemAkademik {
    private static final int ADD_MATKUL = 1;
    private static final int DROP_MATKUL = 2;
    private static final int RINGKASAN_MAHASISWA = 3;
    private static final int RINGKASAN_MATAKULIAH = 4;
    private static final int KELUAR = 5;
    private static Mahasiswa[] daftarMahasiswa = new Mahasiswa[100];
    private static MataKuliah[] daftarMataKuliah = new MataKuliah[100];
    
    private Scanner input = new Scanner(System.in);

    private Mahasiswa getMahasiswa(long npm) {
        for (int i=0; i<daftarMahasiswa.length;i++){
            if (daftarMahasiswa[i].getNpm() == npm){
                return daftarMahasiswa[i];
            }
        }
        return null;
    }

    private MataKuliah getMataKuliah(String namaMataKuliah) {
        for (int i=0; i<daftarMataKuliah.length;i++){
            if (daftarMataKuliah[i].toString().equals(namaMataKuliah)){
                return daftarMataKuliah[i];
            }
        }
        return null;
    }


    private void addMatkul(){
        System.out.println("\n--------------------------ADD MATKUL--------------------------\n");
        System.out.print("Masukkan NPM Mahasiswa yang akan melakukan ADD MATKUL : ");

        long npm = Long.parseLong(input.nextLine());
        Mahasiswa mhs = getMahasiswa(npm);

        System.out.print("Banyaknya Matkul yang Ditambah: ");
        int banyakMatkul = Integer.parseInt(input.nextLine());
        System.out.println("Masukkan nama matkul yang ditambah");
        for(int i=0; i<banyakMatkul; i++){
            System.out.print("Nama matakuliah " + i+1 + " : ");
            String namaMataKuliah = input.nextLine();
            mhs.addMatkul(getMataKuliah(namaMataKuliah));
        }
        System.out.println("\nSilakan cek rekap untuk melihat hasil pengecekan IRS.\n");
    }

    private void dropMatkul(){
        System.out.println("\n--------------------------DROP MATKUL--------------------------\n");

        System.out.print("Masukkan NPM Mahasiswa yang akan melakukan DROP MATKUL : ");
        long npm = Long.parseLong(input.nextLine());

        Mahasiswa mhs = getMahasiswa(npm);

        if (mhs.getJumlahMatkulDiambil() == 0){
            System.out.println("[DITOLAK] Belum ada mata kuliah yang diambil.");
        }
        else{
            System.out.print("Banyaknya Matkul yang Di-drop: ");
            int banyakMatkul = Integer.parseInt(input.nextLine());
            System.out.println("Masukkan nama matkul yang di-drop:");
            for(int i=0; i<banyakMatkul; i++){
                System.out.print("Nama matakuliah " + i+1 + " : ");
                String namaMataKuliah = input.nextLine();
                mhs.dropMatkul(getMataKuliah(namaMataKuliah));
            }
            System.out.println("\nSilakan cek rekap untuk melihat hasil pengecekan IRS.\n");
        } 
    }


    private void ringkasanMahasiswa(){
        System.out.print("Masukkan npm mahasiswa yang akan ditunjukkan ringkasannya : ");
        long npm = Long.parseLong(input.nextLine());

        Mahasiswa mhs = getMahasiswa(npm);
        System.out.println("\n--------------------------RINGKASAN--------------------------\n");
        System.out.println("Nama: " + mhs);
        System.out.println("NPM: " + npm);
        System.out.println("Jurusan: " + mhs.getJurusan());
        System.out.println("Daftar Mata Kuliah: ");

   
        if(mhs.getJumlahMatkulDiambil() == 0){
            System.out.println("Belum ada mata kuliah yang diambil.");
        }
        else{
            for(int i=0, j=0; i<mhs.getDaftarMataKuliah().length; i++){
                if (mhs.getDaftarMataKuliah()[i] != null){
                    System.out.println ((j+1) + ". " + mhs.getDaftarMataKuliah()[i]);
                    j++;
                }
            }
        }

        System.out.println("Total SKS: " + mhs.getTotalSKS());
        System.out.println("Hasil Pengecekan IRS:");
        mhs.cekIRS(); 

        if (mhs.getMasalahIRS()[0] == null && mhs.getMasalahIRS()[1]==null){
            System.out.println("IRS tidak bermasalah.");
        }
        else{
            for(int i=0, j=0; i < mhs.getMasalahIRS().length; i++){
                if (mhs.getMasalahIRS()[i] != null){
                    System.out.println ((j+1) + ". " + mhs.getMasalahIRS()[i]);
                    j++;
                }
            }
        } 
    }


    private void ringkasanMataKuliah(){
        System.out.print("Masukkan nama mata kuliah yang akan ditunjukkan ringkasannya : ");
        String namaMataKuliah = input.nextLine();
        
        MataKuliah matkul = getMataKuliah(namaMataKuliah); //bikin objek matakuliah
        System.out.println("\n--------------------------RINGKASAN--------------------------\n");
        System.out.println("Nama mata kuliah: " + namaMataKuliah);
        System.out.println("Kode: " + matkul.getKode());
        System.out.println("SKS: " + matkul.getSks());
        System.out.println("Jumlah mahasiswa: " + matkul.getJumlahMahasiswa());
        System.out.println("Kapasitas: " + matkul.getKapasitas());
        System.out.println("Daftar mahasiswa yang mengambil mata kuliah ini: ");

        if (matkul.getJumlahMahasiswa() == 0){
            System.out.println("Belum ada mahasiswa yang mengambil mata kuliah ini.");
        }
        else{
            for (int i = 0, j=0; i < matkul.getDaftarMahasiswa().length; i++){
                if (matkul.getDaftarMahasiswa()[i] != null){
                    System.out.println((j+1) + ". " + matkul.getDaftarMahasiswa()[i]);
                    j++;
                }
            }
        }
    }

    private void daftarMenu(){
        int pilihan = 0;
        boolean exit = false;
        while (!exit) {
            System.out.println("\n----------------------------MENU------------------------------\n");
            System.out.println("Silakan pilih menu:");
            System.out.println("1. Add Matkul");
            System.out.println("2. Drop Matkul");
            System.out.println("3. Ringkasan Mahasiswa");
            System.out.println("4. Ringkasan Mata Kuliah");
            System.out.println("5. Keluar");
            System.out.print("\nPilih: ");
            try {
                pilihan = Integer.parseInt(input.nextLine());
            } catch (NumberFormatException e) {
                continue;
            }
            System.out.println();
            if (pilihan == ADD_MATKUL) {
                addMatkul();
            } else if (pilihan == DROP_MATKUL) {
                dropMatkul();
            } else if (pilihan == RINGKASAN_MAHASISWA) {
                ringkasanMahasiswa();
            } else if (pilihan == RINGKASAN_MATAKULIAH) {
                ringkasanMataKuliah();
            } else if (pilihan == KELUAR) {
                System.out.println("Sampai jumpa!");
                exit = true;
            }
        }
    }


    private void run() {
        System.out.println("====================== Sistem Akademik =======================\n");
        System.out.println("Selamat datang di Sistem Akademik Fasilkom!");
        
        System.out.print("Banyaknya Matkul di Fasilkom: ");
        int banyakMatkul = Integer.parseInt(input.nextLine());
        System.out.println("Masukkan matkul yang ditambah");
        System.out.println("format: [Kode Matkul] [Nama Matkul] [SKS] [Kapasitas]");

        for(int i=0; i<banyakMatkul; i++){
            String[] dataMatkul = input.nextLine().split(" ", 4);
            int sks = Integer.parseInt(dataMatkul[2]);
            int kapasitas = Integer.parseInt(dataMatkul[3]);
            String kode = dataMatkul[0];
            String nama = dataMatkul[1];
            MataKuliah matkul = new MataKuliah(kode, nama, sks, kapasitas);
            daftarMataKuliah[i] = matkul;
        }

        System.out.print("Banyaknya Mahasiswa di Fasilkom: ");
        int banyakMahasiswa = Integer.parseInt(input.nextLine());
        System.out.println("Masukkan data mahasiswa");
        System.out.println("format: [Nama] [NPM]");

        for(int i=0; i<banyakMahasiswa; i++){
            String[] dataMahasiswa = input.nextLine().split(" ", 2);
            long npm = Long.parseLong(dataMahasiswa[1]);
            String nama = dataMahasiswa[0];
            Mahasiswa mhs = new Mahasiswa(nama, npm);
            daftarMahasiswa[i] = mhs;
        }
        daftarMenu();
        input.close();
    }

    public static void main(String[] args) {
        SistemAkademik program = new SistemAkademik();
        program.run();
    }
}
