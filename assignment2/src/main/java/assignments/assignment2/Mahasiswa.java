package assignments.assignment2;

public class Mahasiswa {
    private MataKuliah[] daftarMataKuliah;
    private String[] masalahIRS;
    private int totalSKS;
    private String nama;
    private String jurusan;
    private String kodeJurusan;
    private long npm;
    private String npmString;
    private int jumlahMatkulMhs;

    public Mahasiswa(String nama, long npm){
        this.nama = nama;
        this.npm = npm;
        this.npmString = Long.toString(npm);
        kodeJurusan = npmString.substring(2,4);
        if(kodeJurusan.equals("01")){
            jurusan = "Ilmu Komputer";
        }
        else{
            jurusan = "Sistem Informasi";
        }
        daftarMataKuliah = new MataKuliah [10];
        totalSKS = 0;
        jumlahMatkulMhs = 0; 
    }

    public long getNpm(){
        return npm;
    }

    public String getNama(){
        return nama;
    }

    public int getTotalSKS(){
        return totalSKS;
    }

    public String getJurusan(){
        return jurusan;
    }

    public MataKuliah[] getDaftarMataKuliah(){
        return this.daftarMataKuliah;
    }

    public String[] getMasalahIRS(){
        return masalahIRS;
    }

    public int getjumlahMatkulMhs(){
        return jumlahMatkulMhs;
    }


    public void addMatkul(MataKuliah mataKuliah){
        boolean matkulSudahDiambil = false;
        for (int i=0; i < daftarMataKuliah.length; i++){
            if (daftarMataKuliah[i] != null && daftarMataKuliah[i] == mataKuliah){
                System.out.println("[DITOLAK] " + mataKuliah.getNama() + " telah diambil sebelumnya.");
                matkulSudahDiambil = true;
                break;
            }
        }

        if (matkulSudahDiambil == false){
            if (mataKuliah.getJumlahMahasiswa() == mataKuliah.getKapasitas()){
                System.out.println ("[DITOLAK] " + mataKuliah.getNama() + " telah penuh kapasitasnya.");
            }
            else{
                if (jumlahMatkulMhs >= 10){
                    System.out.println("[DITOLAK] Maksimal mata kuliah yang diambil hanya 10.");
                }
                else{
                    daftarMataKuliah[jumlahMatkulMhs] = mataKuliah;
                    jumlahMatkulMhs++;
                    totalSKS += mataKuliah.getSks();
                    mataKuliah.addMahasiswa(this);
                }
            }
        }
    }

    public void dropMatkul(MataKuliah mataKuliah){
        MataKuliah[] matkulFix = new MataKuliah [10];

        for (int i=0; i < daftarMataKuliah.length; i++) {
            if (daftarMataKuliah[i] != null && daftarMataKuliah[i] != mataKuliah) {
                matkulFix[i] = daftarMataKuliah[i];
            }
        }

        boolean udahPernah = false; 
        for (int i=0; i<daftarMataKuliah.length;i++){
            if (daftarMataKuliah[i] == mataKuliah) {
                udahPernah = true; 
            }
        }
        boolean bisaDrop = true;
        if (udahPernah == false){
            System.out.println("[DITOLAK] " + mataKuliah.getNama() + " belum pernah diambil.");
            bisaDrop = false;
        }

        if (bisaDrop == true){
            mataKuliah.dropMahasiswa(this); 
            totalSKS = totalSKS - mataKuliah.getSks();
            jumlahMatkulMhs--;
        }

        this.daftarMataKuliah = matkulFix; 
    }

    public void cekIRS(){
        masalahIRS = new String[20];
        if (totalSKS > 24){
            masalahIRS[0] = "SKS yang Anda ambil lebih dari 24";
        }

        for (int k= 0; k < daftarMataKuliah.length; k++){
            if (daftarMataKuliah[k] != null) {
                if (daftarMataKuliah[k].getKode().equals("SI") && this.jurusan.equals("Ilmu Komputer")) {
                    for (int i = 0; i < masalahIRS.length; i++) {
                        if (this.masalahIRS[i] == null) {
                            masalahIRS[i] = ("Mata Kuliah" + daftarMataKuliah[k].getNama() + " tidak dapat diambil jurusan IK");
                            break;
                        }
                    }
                }
                else if (daftarMataKuliah[k].getKode().equals("IK") && this.jurusan.equals("Sistem Informasi")) {
                    for (int i=0; i<masalahIRS.length; i++){
                        if (this.masalahIRS[i] == null) {
                            masalahIRS[i] = ("Mata Kuliah " + daftarMataKuliah[k].getNama() + " tidak dapat diambil jurusan SI");
                            break;
                        }
                    }
                }
            }
        }
    }

    public String toString() {
        return this.nama;
    }

}
