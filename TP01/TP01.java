import java.util.Scanner;

class TP01{
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        long npm = in.nextLong();
        if(validation(npm)){
            System.out.println(extraction(npm));}
        else{
            System.out.println("NPM tidak valid!");}
    }
    
    public static String string_npm(long npm){
        String npmString = String.valueOf(npm);
        return npmString;
    }

    public static int part_npm(long npm, int indexa, int indexb){
        int npmPart = Integer.parseInt(string_npm(npm).substring(indexa,indexb));
        return npmPart;
    }

    public static String tahun_npm(long npm){
        String tahun = "20"+string_npm(npm).substring(0,2);
        return tahun;
    }

    public static String jurusan(long npm){
        String kode = string_npm(npm).substring(2,4);
        String jurusan = "";
        if(kode.equals("01")){jurusan = "Ilmu Komputer";}
        else if(kode.equals("02")){jurusan = "Sistem Informasi";}
        else if(kode.equals("03")){jurusan = "Teknologi Informasi";}
        else if(kode.equals("11")){jurusan = "Teknik Telekomunikasi";}
        else if(kode.equals("12")){jurusan = "Teknik Elektro";}
        else{jurusan = "error";}
        return jurusan;
    }

    public static boolean cek_kelahiran(long npm){
        int tanggal = part_npm(npm, 4, 6);
        int bulan = part_npm(npm, 6, 8);
        int tahun = part_npm(npm, 8, 12);
        boolean kabisat = false;
        boolean passed = false;

        //penentuan kabisat
        if(tahun % 400 == 0){kabisat = true;}
        else if(tahun % 100 == 0){kabisat = false;}
        else if(tahun % 4 == 0){kabisat = true;}
        else{kabisat = false;}

        //penentuan sesuai ketentuan tanggal bulan tahun
        if (kabisat == true && bulan == 2){
            if(tanggal <= 29){passed = true;}}
        else{
            if (bulan==2){
                if(tanggal<=28){passed = true;}}
            else if(bulan==1||bulan==3||bulan==5||bulan==7||bulan==8||bulan==10||bulan==12){
                if(tanggal<=31){passed = true;}}
            else{if(tanggal<=30){passed = true;}}}
        return passed;
    }

    public static boolean cek_tahun(long npm){
        int tahun_lahir = part_npm(npm, 8, 12);
        int tahun_npm = Integer.parseInt(tahun_npm(npm));
        boolean passed = false;
        if (tahun_npm - tahun_lahir>=15){passed = true;}
        return passed;
    }

    public static boolean cek_kode(long npm){
        String str_npm = string_npm(npm);
        int pjg = str_npm.length();
        int count = 0;
        for(int i=0;i<(pjg/2)-1;i++){
            int a = str_npm.charAt(i)-48;
            int b = str_npm.charAt((pjg-2)-i)-48;
            count += a*b;
        }

        count += str_npm.charAt(6)-48;
        
        while (count>=10){
            String npmString = String.valueOf(count);
            count = (npmString.charAt(0)-48)+(npmString.charAt(1)-48);
        }
        if(count == str_npm.charAt(13)-48){return true;}
        else{return false;}
    }

    public static boolean validation(long npm){
        String str_npm = string_npm(npm);
        if(str_npm.length()!=14){return false;}
        if(jurusan(npm).equals("error")){return false;}
        if(cek_tahun(npm)==false){return false;}
        if(cek_kelahiran(npm)==false){return false;}
        if(cek_kode(npm)==false){return false;}
        return true;
    }

    public static String extraction(long npm){
        String result = "";
        String tahun = "Tahun masuk: "+tahun_npm(npm);
        String jurusan = "jurusan: "+jurusan(npm);
        String ttl = "Tanggal Lahir: "+String.format("%02d",part_npm(npm, 4, 6))+"-"+String.format("%02d",part_npm(npm, 6, 8))+"-"+part_npm(npm, 8, 12);
        result = tahun+"\n"+jurusan+"\n"+ttl;
        return result;
    }


    
}